from __future__ import print_function
import sys
from time import sleep
from sys import stdin, exit
from PodSixNet.Connection import connection, ConnectionListener
from _thread import *

class Client(ConnectionListener):
    def __init__(self, host, port):
        self.Connect((host, port))
        print("Chat client started")
        print("Ctrl-C to exit")
        print("Enter your nickname: ")
        connection.Send({"action": "nickname", "nickname": stdin.readline().rstrip("\n")})
        t = start_new_thread(self.InputLoop, ())
    def Loop(self):
        connection.Pump()
        self.Pump()
    def InputLoop(self):
        while 1:
            connection.Send({"action": "message", "message": stdin.readline().rstrip("\n")})
    def Network_players(self, data):
        print("*** players: " + ", ".join([p for p in data['players']]))
    def Network_message(self, data):
        print(data['who'] + ": " + data['message'])
    def Network_connected(self, data):
        print("You are now connected to the server")
    
    def Network_error(self, data):
        print('error:', data['error'][1])
        connection.Close()
    def Network_disconnected(self, data):
        print('Server disconnected')
        exit()
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage:", sys.argv[0], "host:port")
        print("e.g.", sys.argv[0], "localhost:31425")
    else:
        host, port = sys.argv[1].split(":")
        c = Client(host, int(port))
        while 1:
            c.Loop()
            sleep(0.001)