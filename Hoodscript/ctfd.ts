// IMPORTS 

import { parse } from "https://deno.land/std/flags/mod.ts";

import {
    Input,
    prompt,
    Secret,
    Select
  } from "https://deno.land/x/cliffy/prompt/mod.ts";

// END IMPORTS


// *-*-*-*-*-*-*-*-*-*


// TYPE DEFINITIONS

type Nullable<T> = T | null | undefined;

type question_id = number;

type challengeMap = Map<question_id, Challenge>;

type categoriesMap = Map<string, Array<question_id>>;

type solvesMap = Map<question_id, Solve>;

type solution = {
    solved: boolean;
    correct: boolean;
}

// END TYPES


// *-*-*-*-*-*-*-*-*-*


// CLASS DEFINITIONS

class Challenge {
    category: string = "";
    description: string = "";
    name: string = "";
    id: number = -1;
    type: string = "";
    public answered: solution = {solved: false, correct: false};
}

class Solve {
    category: string = "";
    challenge_id: number = -1;
    type: string = "";
}

class AnswerResponse {
    status: string = "";
    message: string = "";
}

class User {
    name: string = "";
    place: string = "";
    score: number= -1;

    public toString() {
        return `Username: ${this.name}\nPlace: ${this.place}\nScore: ${this.score}`;
    }
}

class Questions {

    private challenges: challengeMap = new Map<question_id, Challenge>();
    private categories: categoriesMap = new Map<string, Array<question_id>>();


    public setChallenge(chal?: Challenge){
        if(chal) {
            if(!this.challenges.has(chal.id)){
                this.challenges.set(chal.id, chal);
            }
                
        } else{
            throw(new Error("Null Challenge Received"));
        }

        if(debug){
            console.log(`getQuestions, Verify Challenge set correctly: `);
            console.log(`Challenge: ${this.challenges.get(chal.id!)!}`);
        }
    }

    public setCategory(chal?: Challenge){
        if(chal){
            // Create new array if mapping does not yet exist
            if(!this.categories.has(chal.category)){
                this.categories.set(chal.category!, new Array<question_id>());
            } 

            // Check if value already exist in categories array
            if(!this.categories.get(chal.category)!.includes(chal.id)){
                this.categories.get(chal.category)!.push(chal.id);
            }
            
        } else {
            throw(new Error("Null Challenge Received"));
        }

        if(debug){
            console.log(`getQuestions, Verify Challenge set correctly: `);
            console.log(`Category: ${this.categories.get(chal.category!)!.indexOf(chal.id!)}`);
        }
    }

    public setAnswered(solve?: Solve){
        if(solve) {
            if(this.challenges.has(solve.challenge_id)){
                if(solve.type = "correct"){
                    this.challenges.get(solve.challenge_id)!.answered = {solved: true, correct: true};
                } else {
                    // TODO - given that the solves API only returns challenges with correct solutions
                    // This logic could be false. Need to verify. This will most likely never be hit.
                    this.challenges.get(solve.challenge_id)!.answered = {solved: true, correct: false};
                }
            } else {
                throw(new Error(`Unable to set solve: ${solve.challenge_id}. Client potentially out of sync.`));
            }
        }
    }

    private setSolved(id: question_id, ans: solution){
        if(this.challenges.has(id)){
            this.challenges.get(id)!.answered = ans;
        } else{
            throw("Unable to set answer - challenge does not exist in internal data");
        }
    }

    public async promptCategories(){

        if(debug) {
            console.debug("promptCategories DEBUG: ")
        }

        var catArr = new Array<string>();

        for(const [k, ] of this.categories){
            if(debug){
                console.debug(`Key: ${k}, "value": ${this.categories.get(k)}`);
            }
            catArr.push(k);
        }

        // sort the array so categories are posted in alphabetical order
        catArr.sort();

        var json = '{ "message": "Category", "options": [';

        for(const cat of catArr){
            
            if(debug){
                console.debug(`{"name": "${cat}", "value": "${cat}"},`);
            }

            json = json.concat(`{"name": "${cat}", "value": "${cat}"},`);
        }

        json = json.concat('{"name": "Go Back", "value": "back"}');

        json = json.concat(']}');

        if(debug){
            console.debug(json);
        }

        while(true){
            
            // TODO: Manipulate Object directly instead of parsing and creating JSON each time 
            // The function is called

            let selection = await Select.prompt(JSON.parse(json));

            if(this.categories.has(selection)){
                await this.promptQuestions(selection);
            } else {
                switch(selection){
                    case 'back': {
                        return;
                    }
                    default: {
                        console.log("How did you get here?");
                        continue;
                    }
                }
            }
        }
    }

    private async genQuestionsJson(category: string): Promise<string> {
        
        var ids = this.categories.get(category);

        if(!ids){
            throw new Error(`Invalid Category: '${category}' was selected.`);
        }

        var json = '{ "message": "Select Question", "options": [';

        var idsArr = new Array<question_id>();
        
        for(const id of ids){
            idsArr.push(id);
        }

        idsArr.sort();

        for(const id of idsArr){
            var question = this.challenges.get(id);
            if(question){
                if(question.answered.correct == true){
                    json = json.concat((`{"name": "✅ ${question.name} (Solved)", "value": "${question.id}"},`));
                } else {
                    json = json.concat(`{"name": "${question.name}", "value": "${question.id}"},`);
                }
            }
        }

        json = json.concat('{"name": "Go Back", "value": "back"}');
        json = json.concat(']}');
        
        if(debug){
            console.debug(json);
            await Deno.writeTextFile('./questions.json', json);
        }

        return json;
    }

    private async promptQuestions(category: string) {
    
        let json = await this.genQuestionsJson(category);

        while(true) {

            let promptObject = JSON.parse(json);

            if(!promptObject){
                throw(new Error("Unable to parse questions list: JSON: " + json));
            }
            
            let selection = await Select.prompt(promptObject);
            

            let id = parseInt(selection);

            if(debug){
                console.debug(`Selection: ${selection}, id: ${id}`);
            }

            if(this.challenges.has(id)){
                try {
                    await this.promptChallenge(id);
                    
                    // Regenerate json to display correctly answered questions
                    json = await this.genQuestionsJson(category); 

                    // Format questions list 
                    // TODO: Work question storage object directly to prevent needing to create everytime
                    // function is called 

                } catch(e) {
                    console.log(e.message);
                }
            } else {
                switch(selection){

                    case 'back': {
                        return;
                        break;
                    }
                    default: {
                        console.log("How did you get here?");
                        continue;
                    }
                }
            }
        }
    }

    private async fetchChallenge(id: question_id){

        try { 
            headers.set("content-type", "application/json");
    
            res = await fetch(url+'/api/v1/challenges/' + id, {
                method: "GET",
                headers: headers
            });

            let json = await res.text();

            if(debug){
                console.debug(`JSON: ${json}`);
            }
            let data = JSON.parse(json).data;

            let challenge: Challenge = Object.assign(new Challenge, data);

            if(!challenge){
                throw(new Error("Unable to parse challenge, try again."));
            }

            this.challenges.get(id)!.description = challenge.description;
            this.challenges.get(id)!.type = challenge.type;


            if(debug){
                console.debug("Prompt: " + this.challenges.get(id)!.description);
            }


        } catch(e){
            throw(e);
        }
    }

    private async promptChallenge(id: question_id){

        if(debug){
            console.debug("promptChallenge DEBUG: ");
        }

        if(!this.challenges.has(id) || this.challenges.get(id)!.description == ""){
            try {
                await this.fetchChallenge(id);
            } catch(e){
                throw(e);
            }
        }

        let challenge = this.challenges.get(id);


        if(challenge){

            switch(challenge!.type){

                case 'standard': {
                    
                    let chalPrompt = "";

                    let solved = false;

                    // Some descriptions include HTML formatted text. This needs to be removed for clean formatting

                    while(!solved){

                        if(debug){
                            console.log("Challenge Object: " + JSON.stringify(challenge));
                        }
                        if (challenge!.description.includes('<p>')){
                            let matches = challenge!.description.match(/(?<=<p>).*(?=<\/p>)/g); // Get string from in between paragraph tags
                            
                            if(matches){
                                chalPrompt = matches![0];
                            } else {
                                throw(new Error("Regex failed to match: " + chalPrompt));
                            }
                        } else {
                            chalPrompt = challenge.description;
                        }

                        chalPrompt = chalPrompt.concat("\n(Type '..' and press Enter to go back without answering.)\n\n");

                        let answer = await Input.prompt(
                            {
                                message: chalPrompt
                            });
                        
                            if(debug){
                                console.debug("User Answer: " + answer);
                            }
            
                            if(answer == ".."){
                                return;
                            }

                            solved = await this.postAnswer(id, answer);

                            if(!solved) {
                                console.log("Try again.\n");                                
                            }
                        }

                    break;
                }

                case 'multiple_choice': {

                    // remove carriage returns and newlines from description
                    let description = challenge!.description.replace(/[\r\n*]/g, '');
                    
                    if(debug){
                        console.debug("Prompt after Regex: " + JSON.stringify(description));
                    }

                    // chalPrompt = chalPrompt.replace(/\n/g, '');

                    let options = description.split("()");

                    if(!options){
                        throw(new Error("Unable to parse question, try again."));
                    }

                    let chalPrompt = options[0]; // First split item will be the question prompt
                    options = options.slice(1); // Take remaining items as options for the prompt

                    // Some descriptions include HTML formatted text. This needs to be removed for clean formatting
                    if (chalPrompt.includes('<p>')){
                        let matches = chalPrompt.match(/(?<=<p>).*(?=<\/p>)/g); // Get string from in between paragraph tags
                        
                        if(matches){
                            chalPrompt = matches![0];
                        } else {
                            throw(new Error("Regex failed to match: " + chalPrompt));
                        }
                    }

                    var prompt = `{ "message": "${chalPrompt}", "options": [`;

                    for(let o of options){
                        o = o.trim(); // strip leading spaces
                        
                        prompt = prompt.concat(`{"name": "${o}", "value": "${o}"},`);
                    }

                    prompt = prompt.concat('{"name": "Go Back", "value": "back"}');
                    prompt = prompt.concat(']}');

                    if(debug){
                        console.log(prompt);
                    }

                    let promptObject = JSON.parse(prompt);

                    let answer = await Select.prompt(promptObject);

                    if(answer == 'back'){
                        return;
                    }

                    await this.postAnswer(id, answer);
                    
                    break;
                }

                default: {
                    throw( new Error ("Invalid Challenge Type: " + challenge!.type));
                }
            }
        } else {
            console.log("Unable to fetch challenge. Try again later.");
            return;
        }
    }

    private async postAnswer(id: question_id,answer: string): Promise<boolean>{
        
        if(debug){
            console.debug("postAnswer DEBUG");
        }

        res = await fetch(url +'/api/v1/challenges/attempt', {
            method: "POST",
            headers: headers,
            body: `{"challenge_id": ${id}, "submission": "${answer}"}`,
        });

        if(debug){
            console.log("Response Body: " + `{"challenge_id": ${id}, "submission": "${answer}"}`);
        }

        let json = await res.text();

        if(debug){
            console.debug(`Response JSON: ${json}`);
        }

        let data: AnswerResponse = Object.assign(new AnswerResponse, JSON.parse(json).data);

        let solved = false;

        if(data){
            switch(data.status){
                case "already_solved": {
                    console.log(data.message + "\n");
                    this.setSolved(id, {solved: true, correct: true});
                    solved = true;
                    break;
                }
                case "correct": {
                    console.log(data.message + "\n");
                    this.setSolved(id, {solved: true, correct: true});
                    solved = true;
                    break;
                }
                case "incorrect": {
                    console.log(data.message + "\n");
                    this.setSolved(id, {solved: true, correct: false});
                    solved = false;
                    break;
                }
            }
        }

        return solved;
    }
}

// END CLASS DEFINITIONS


// *-*-*-*-*-*-*-*-*-*


// GLOBAL VARIABLES

var url = "http://10.50.22.22:8000";

var res : Response;

const headers = new Headers();
headers.set('Content-Type', 'application/x-www-form-urlencoded');
headers.set('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0');
headers.set('Connection', 'keep-alive');
headers.set('credentials', 'include');

var questions = new Questions();

var userName: string | null | undefined = null;

var debug = false;

var user: Nullable<User> = null;


// END GLOBAL VARIABLES

// *-*-*-*-*-*-*-*-*-*


// HELPER FUNCTIONS

const getCookieAndNonce = async (res: Response) => {

    let nonce_regex = /(?<=nonce = ").*(?=")/g;

    let cookie = res.headers.get('set-cookie')?.split(';')[0];
    let nonces = (await res.text()).match(nonce_regex);

    var nonce;

    if(nonces){
        nonce = nonces[0];
    } else {
        nonce = null;
    }

    return [cookie, nonce];
}


const login = async () => {
    
    // Prompt for user name and password
    const user = await prompt ([
        {
            name: 'user',
            message: 'Username: ',
            type: Input,
        }, 
        {
            name: 'pass',
            message: 'Password',
            type: Secret
        }
    ]);

    userName = user.user;

    try {

        if(headers.has('cookie')){
            headers.delete('cookie');
        }
        
        // Get cookie and nonce required for login request 
        res = await fetch(url +'/login', {
            method: "GET",
            headers: headers
        });
        
        
        var [cookie, nonce] = await getCookieAndNonce(res);
        
        if((headers.get("cookie") == null && cookie == null) || nonce == null){
           throw new Error("Bad Response from server - reattempting login.")
        }

        headers.set('cookie', cookie!);
        
        // Attempt to login user
        res = await fetch(url +'/login', {
            method: "POST",
            headers: headers,
            body: `name=${user.user}&password=${user.pass}&nonce=${nonce}`,
            redirect: "manual"
        });
        
        // Get (potentially) authenticated cookie if login was successful
        [cookie, ] = await getCookieAndNonce(res);

        if(cookie) {
            // Set cookie if the request was successfull
            headers.set("cookie", cookie);
            try { 
                var [_, err] = await getMe();
                if(err) {
                    throw(err)
                } else{
                    console.log(`Login successful! Welcome ${user.user}!`);
                }
            } catch(e) {
                throw(e);
            }   
        } else {
            throw new Error('Login attempt failed. Check credentials.');
        }

        // fetch CSRF-Token requeired for posting answers to challenges

        res = await fetch(url +'/challenges', {
            method: "GET",
            headers: headers,
            redirect: "manual"
        });

        let html = await res.text();

        if(html){
            let csrfRegex = /(?<=csrf_nonce = ").*(?=")/
            let csrfs = html.match(csrfRegex);

            if(csrfs){
                headers.set("CSRF-Token", csrfs[0]);
            } else {
                throw(new Error("Unable to set CSRF Token"));
            }
        }

    } catch(e){
        throw(e);
    }
}


const getMe = async (): Promise<[Nullable<User>, Nullable<Error>]> => {
    try {

        headers.set("content-type", "application/json");

        res = await fetch(url+'/api/v1/users/me', {
            method: "GET",
            headers: headers
        });

        var text = await res.text();
        
        //DEBUG
        if(debug){
            console.log(`getME: ${text}`);
        }
        

        if (userName && text.includes(userName)) {    
             // DEBUG
            var userData = JSON.parse(text).data;
            userData = Object.assign(new User, userData);

            if(debug){
                console.debug("DEBUG getMe(): " + userData.toString());
            }

            return [userData, null];

        } else {
            return [null, new Error('Unable to fetch user data from /api/v1/users/me')];
        }

    } catch(e) {
        throw e; 
    }
}

const getSolves = async () => {
    try {

        headers.set("content-type", "application/json");

        res = await fetch(url+'/api/v1/users/me/solves', {
            headers: headers
        });

        const text = await res.text();
        var data = JSON.parse(text).data;

        for(const d of data){
            let solve = Object.assign(new Solve, d);
            questions.setAnswered(solve);
        }

    } catch(e) {
        throw e;
    }
}

const fetchData = async () => {

    try { 
        headers.set("content-type", "application/json");

        res = await fetch(url+'/api/v1/challenges', {
            method: "GET",
            headers: headers
        });

        var text = await res.text(); 

        var data = JSON.parse(text).data;

        if(debug){
            console.debug(`fetchData, Received text: ${text}`);
            console.debug(`fetchData, Parsed Challenge Data: ${JSON.stringify(data)}`);
        }

        for( const d of data){

            try {

                let chal = Object.assign(new Challenge, d);

                if(debug){
                    console.debug(`fetchData, Challenge JSON: ${JSON.stringify(d)}`)
                    console.debug(`fetchData, Challenge ID: ${chal.id}`);
                    console.debug(`fetchData, Challenge Category: ${chal.category}`);
                    console.debug(`fetchData, Challenge Name: ${chal.name}`);
                }

                questions.setChallenge(chal);
                questions.setCategory(chal);

            } catch(e) {
                console.log(e);
                continue;
            }
        }

        await getSolves();

    } catch(e){
        throw(e);
    }
    
}

// END HELPER FUNCTIONS





(async function() {

    var args = parse(Deno.args);

    // enable debugging mode if flag is set
    if(args.debug){
        debug = args.debug;
    }

    // DEBUG
    if(debug) {
        console.log("Debugging enabled.");
        console.log(`Args ${args}`);
    }

    if(args.addr){
        url = args.addr
        if(debug) {
            console.log(`Url: ${url}` )
        }
    }

    if(!args.addr){
        console.log("No address provided - connecting to default.")
    }

    console.log(`Connecting to ${url}`)

    var logged_in = false;

    while(!logged_in) {

        try {
            await login();
            logged_in = true;
        } catch(e) {
            console.log(`${e}`);
        }
    }
    
    try {
        while(true) {
            const activity = await Select.prompt(
                {
                    //type: Select,
                    message: "What would you like to do?",
                    options: [
                        {
                            name: "Challenges", value: "challenges"
                        }, {
                            name: "User Info", value: 'getMe'
                        }, {
                            name: "Quit", value: 'quit'
                        }
                    ],
                }
            );
            
            switch(activity) {
                case 'getMe': {
                    var err = null;
                    [user, err] = await getMe();
                    if(err){
                        throw(new Error("Issue fetching user information: " + err));
                    }
                    console.log(user!.toString());
                    break;
                }
                case 'getSolves': {
                    console.log(await getSolves());
                    break;
                }
                case 'challenges': {
                    //console.log
                    await fetchData();
                    await questions.promptCategories();
                    break;
                }
                case 'quit': {
                    console.log("Bye!");
                    return;
                }
            }
        }
    } catch (e) {
        console.log(e); 
    }
})();

