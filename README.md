# Project Portfolio

Welcome to my project portfolio! This repository showcases a collection of projects I have worked on, demonstrating my skills and interests in various domains. Each project provides a brief description and relevant information to help you understand its purpose and functionality.

## Projects

### ADSB

ADSB is a Python tool designed to encode/decode ADSB-out messages. It serves as a valuable resource for systems that rely on ADSB data. This tool enables users to effectively process and utilize ADSB messages for their specific requirements.

### Python Network Chat

Python Network Chat is a network-based chat application that allows users to connect with individuals on the same network. Using Python sockets, users can engage in text-based conversations in real-time. This project demonstrates my proficiency in networking concepts and Python programming.

### Python Network Whiteboard

Similar to the Python Network Chat project, Python Network Whiteboard provides a collaborative environment for users connected over a network. However, instead of text-based communication, it offers a whiteboard interface. Users can interact and draw together, making it an ideal solution for remote collaboration or brainstorming sessions.

### Python Based Minecraft Clone

In this project, I leveraged Ursina, a Python module, to create a Minecraft-like game. While it doesn't replicate the entire Minecraft experience, it focuses on recreating core functionalities using a 3D game engine.

### Hoodscript

Hoodscript is a CLI-based interface project that interacts with a ctfd webserver. It offers a convenient way to manage and solve challenges within the ctfd framework. With this project, I aimed to streamline the process of accessing and interacting with ctfd challenges through a command-line interface.


## Contributions

I welcome contributions, feedback, and suggestions for any of the projects in this portfolio. If you find any issues or have ideas for improvements, please feel free to open an issue or submit a pull request. Let's collaborate and make these projects even better together!

## License

Unless otherwise stated, all projects in this portfolio are open source and released under the [MIT License](https://opensource.org/licenses/MIT). Please refer to each project's directory for specific licensing information.

---

Thank you for visiting my project portfolio! If you have any questions or would like to discuss any of the projects further, feel free to reach out to me.
