from re import S, T
from ursina import *
from ursina.prefabs.first_person_controller import FirstPersonController
from os import listdir
from scipy import ndimage
import numpy as np
import time
from perlin_noise import PerlinNoise
app = Ursina()

terrain = Entity(model=None, collider=None)

noise = PerlinNoise(octaves=2, seed=100)
freq = 40
amp = 5
terrain_width = 40


class Inventory(Entity):
    
    def __init__(self, **kwargs):
        super().__init__(
            parent = camera.ui,
            model = Quad(radius=.015),
            texture = 'white_cube',
            texture_scale = (8,1),
            scale = (.8, .1),
            origin = (-.5, .5),
            position = (-.75,-.4),
            color = color.color(0,0,.1,.9),
            )

        for key, value in kwargs.items():
            setattr(self, key, value)


    def find_free_spot(self):
        for y in range(8):
            for x in range(5):
                grid_positions = [(int(e.x*self.texture_scale[0]), int(e.y*self.texture_scale[1])) for e in self.children]
                # print(grid_positions)

                if not (x,-y) in grid_positions:
                    # print('found free spot:', x, y)
                    return x, y
    


    def append(self, item, x=0, y=0):

        if len(self.children) >= 5*8:
            print('inventory full')
            error_message = Text('<red>Inventory is full!', origin=(0,-1.5), x=-.5, scale=2)
            destroy(error_message, delay=1)
            return

        x, y = self.find_free_spot()

        icon = Entity(
            parent = self,
            model = 'assets/block',
            texture = item,
            color = color.white,
            scale_x = 0.5/self.texture_scale[0],
            scale_y = 0.5/self.texture_scale[1],
            origin = (-1,1),
            x = x * 1/self.texture_scale[0],
            y = -y * 1/self.texture_scale[1],
            z = -.5,
            alpha = 0.5
            )
        # name = item.replace('_', ' ').title()
        icon.tooltip = Tooltip(item)
        icon.tooltip.background.color = color.color(0,0,0,.8)

inventory = Inventory()
window.fps_counter.enabled = False
window.exit_button.visible = False
if sys.version_info[0] >= 3:
    xrange = range

blocks = []
punch = Audio('assets/punch', autoplay=False)
for i in os.listdir('assets'):
    if 'png' in i: 
        blocks.append(load_texture(i))
block = blocks

# [
#     load_texture('assets/grass.png'),
#     load_texture('assets/stone.png'),
#     load_texture('assets/gold.png'),  
#     load_texture('assets/lava.png'),]
blocks = [load_texture('assets/grass.png'),
    load_texture('assets/stone.png'),
    load_texture('assets/gold.png'),
    load_texture('assets/lava.png'),]

for i in block:
    inventory.append(i)
    
player = FirstPersonController()
block_id = 1
def input(key):
    global block_id, hand
    if key == 'escape':
        exit()

    if key.isdigit():
        for i in inventory.children:
            inventory.children[inventory.children.index(i)].alpha=0.5
        block_id = int(key)
        if block_id >= len(blocks):
            block_id = len(blocks) - 1
        hand.texture = blocks[block_id]
        
t=0

sky = Entity(
    parent=scene,
    model='sphere',
    texture=load_texture('assets/sky.jpg'),
    scale=500,
    double_sided=True
)

hand = Entity(
    parent=camera.ui,
    model='assets/block',
    texture = blocks[block_id],
    scale=0.2,
    rotation=Vec3(-10, -10, 10),
    position=Vec2(0.6, -0.3)
)
old = player.x, player.z
t=0.0
def update():
    global t
    t += time.dt
    global old
    hit_info = raycast(player.position, mouse.position, ignore=(player,),distance=5)
    if held_keys['left mouse'] or held_keys['right mouse']:
        punch.play()
        hand.position = Vec2(0.4, -0.5)
    else:
        hand.position = Vec2(0.6, -0.6)

    if held_keys['shift']:
        player.speed = 25
    else:
        player.speed = 10








class Voxel(Button):
    def __init__(self, position=(0, 0, 0), texture='assets/grass.png', done=False, scale_y=0.5):
        super().__init__(
            parent=scene,
            position=position,
            model='assets/block',
            origin_y=0.5,
            texture=texture,
            color=color.color(0, 0, random.uniform(0.9, 1.0)),
            scale=0.5,
            scale_y=scale_y,
            done=done
        )
        

    def input(self, key):
        global t
        if self.hovered:
            dist = distance_xz(player.position, self.position)
            if dist < 5:
                if key == 'left mouse down':
                    destroy(self)
                elif key == 'right mouse down':
                    Voxel(position=self.position + mouse.normal, texture=blocks[block_id])
                            

        
                    
            

player.jump_height=2              


for i in range(terrain_width*terrain_width):
    block = Voxel()
    block.x = floor(i/terrain_width)
    block.z = floor(i%terrain_width)
    block.y = floor(noise([block.x/freq, block.z/freq]) * amp)
    block.parent = terrain

terrain.combine()
terrain.collider = 'mesh'
terrain.texture = 'assets/grass.png'

# for z in range(floor(player.x)+50):
#         for x in range(floor(player.y)+50):
#             voxel = Voxel(position=(x, 0, z))        
                



# o = 10
# for _ in xrange(20):
#     a = random.randint(-o, o)
#     b = random.randint(-o, o)
#     c = -1 
#     h = random.randint(1, 6)
#     s = random.randint(4, 8) 
#     d = 1 
#     texturev = 'assets/grass.png'
#     for y in xrange(c, c + h):
#         for x in xrange(a - s, a + s + 1):
#             for z in xrange(b - s, b + s + 1):
#                 if (x - a) ** 2 + (z - b) ** 2 > (s + 1) ** 2:
#                     continue
#                 if (x - 0) ** 2 + (z - 0) ** 2 < 5 ** 2:
#                     continue
#                 voxel = Voxel(position=(x,y,z), texture=texturev)
#         s -= d  # decrement side length so hills taper off
# collision_zone = CollisionZone(parent=player, radius=5)

app.run()