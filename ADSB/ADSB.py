#!/usr/bin/python3
import random
import socket
import time
import argparse
from copy import deepcopy
from sys import stdout
import textwrap
import signal

# Compact Position Reference Implementation
###########################################

# http://www.anteni.net/adsb/Doc/1090-WP-14-09R1.pdf


from math import floor, pi, acos, cos

# Latitude zones, A-46
NZ = 15

bit17 = 2. ** 17

DlatEven = (360.0 / (4 * NZ))
DlatOdd  = (360.0 / (4 * NZ - 1))
    
def Dlat(i):
    return DlatEven if i == 0 else DlatOdd


# A.1.7.2 d
# Calculate Latitude Zone
def NL(lat):

    if lat == 0:
        return 59

    if lat in [87, -87]:
        return 2

    if lat > 87 or lat < -87:
        return 1

    return floor(2*pi / (acos(1 - (1 - cos(pi / ( 2 * NZ)))/ (cos((pi/180)*lat)) ** 2)))

# A.1.7.3 b / Page A - 47
# Y coordinate within the zone, i is for message type of even (0) or odd (1) - airborne encoding
def YZ(lat, i):
    return floor(bit17 * ((lat % Dlat(i))/ Dlat(i)) + 1/2)

# A.1.7.3 a / Page A - 48
# Rlati (the latitude that a receiving ADS-B system will extract from the transmitted message)
def Rlat(lat, i):
    return Dlat(i) * ((YZ(lat, i) / bit17) + floor(lat / Dlat(i)))



# A.1.7.3 a / Page A - 48
# Dloni (the longitude zone size in the E-W direction) is then computed
def Dlon(lat, i):
    nlcalc = NL(Rlat(lat, i)) - i
    return 360.0 / nlcalc if nlcalc > 0 else 360.0




def cpr_encode(lat, lon, i):
    dlati = Dlat(i)
    dloni = Dlon(lat, i)

    yz = YZ(lat, i)
    
    # A.1.7.3 b / Page A - 48
    xz = floor(2**17 * ((lon % dloni) / dloni) + 0.5)


    # A.1.7.3 a / Page A - 49
    cpr_lat = int(yz) & (2**17-1) # bitwise MOD
    cpr_lon = int(xz) & (2**17-1) # bitwise MOD

    return [cpr_lat, cpr_lon]

##############################################################
# Helper Functions
##################

#Ctrl+C Finder
def handler(signum, frame):
    res = input("Ctrl-c was pressed. Do you really want to exit? y/n ")
    if res == 'y':
        exit(1)

signal.signal(signal.SIGINT, handler)


# convert binary to hex string
def bin2hex(s):
    return hex(int(s, 2))[2:]
    
#Create a line from PointA going a certain "Length" in a certain "Direction" with x planes where x is "Segmentation"
#Direction can be "down" or "right"
#PointA takes a list containing a Longitude and a Latitude.
def line(pointA, length, direction, segmentation: int):
    coords = []
    count = 0
    if direction == 'down':
        for _ in range(segmentation):
            coords.append([pointA[0]-(length/segmentation)*count, pointA[1]])
            count+=1
        return coords
    
    if direction == 'up':
        for _ in range(segmentation):
            coords.append([pointA[0]+(length/segmentation)*count, pointA[1]])
            count+=1
        return coords

    if direction == 'right':
        for _ in range(segmentation):
            coords.append([pointA[0], pointA[1]+(length/segmentation)*count])
            count+=1
        return coords
    if direction == 'left':
        for _ in range(segmentation):
            coords.append([pointA[0], pointA[1]-(length/segmentation)*count])
            count+=1
        return coords

##############################################################
#Constants
##########

PI = '0'*24 # Bits used for parity
NM = 0.016666667; # 1 Nautical Mile (degrees latitude )
downlinkBits = '10001' # Indicates Downlink Format 17
capabilityBits = '111' # Indicates Capability of 7 = Either airborne or on the ground

##############################################################

# dump1090 reference https://mode-s.org/decode/ (Automatic Dependent Surveillance-Broadcast (ADS-B))
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--count', type=int, required=False, default=100, help='Only used with type of "glitch". Controls how many planes to populate.')
parser.add_argument('--mode', type=str, required=False, default='send', help=textwrap.dedent('''Either "send" or "print".
If "print" is selected then all of the raw HEX data will print to the console.
If "send" is selected then all of the data will send to the IP identified with the --port argument.
'''))
parser.add_argument('--type', type=str, required=False, default='normie', help='What shape to populate planes in. options include: "freedom", "smiley", "normie", "glitch"')
parser.add_argument('--addr', type=str, required=False, default='localhost', help="IP address to send data to")
parser.add_argument('--port', type=int, required=False, default=30001, help="Port to send data to")
parser.add_argument('--input', type=str, required=False, default='8==D')
parser.add_argument('--scale', type=float, required=False, default=1.0)
args = parser.parse_args()

if args.mode == 'send':
    s = socket.socket()
    s.connect((args.addr, args.port))


# Cyclic Redundancy Check function. See: https://mode-s.org/decode/content/ads-b/8-error-control.html
def crc(message):
    GENERATOR = '1111111111111010000001001'
    message = list(message)
    for i in range(len(message)-24):
        if message[i] == '1':
            for j in range(len(GENERATOR)):
                message[i+j] = str((int(message[i+j]) ^ int(GENERATOR[j])))
    message = ''.join(message[-24:])
    return message

#If --mode is send, it will send over the socket.
def sendData(data):
    s.send(data.encode())
#If --mode is print, it will print to console.
def printData(data):
    print(data)
#Send the first required packet, Airborne Velocity. See: https://mode-s.org/decode/content/ads-b/5-airborne-velocity.html
def sendAV(i):
    data = '10011001000101001000110100011110100100000000010000111010'
    newPI = crc('{}{}{}{}{}'.format((downlinkBits), (capabilityBits), (i), (data), (PI)))
    data = bin2hex('{}{}{}{}{}'.format((downlinkBits), (capabilityBits), (i), (data), (newPI)))
    if args.mode == 'print':
        data = '*{};'.format(data)
        printData(data)
    elif args.mode == 'send':
        data = '*{};\n'.format(data)
        sendData(data)


#Send the EVEN Airborne Position packet. See: https://mode-s.org/decode/content/ads-b/3-airborne-position.html
def sendeAP(ICAO, latLon):
    tcALT = '010110011000001100000'
    fBit = '0'
    lat = latLon[0]
    lon = latLon[1]
    lat, lon = cpr_encode(lat, lon, 0)
    lat = '{:17b}'.format((lat))
    lon = '{:17b}'.format((lon))    
    lat = lat.replace(' ', '0')
    lon = lon.replace(' ', '0')
    latLon = '{}{}'.format(lat, lon)
    data = '{}{}{}'.format(tcALT, fBit, latLon)
    newPI = crc('{}{}{}{}{}'.format((downlinkBits), (capabilityBits), (ICAO), (data), (PI)))
    data = bin2hex('{}{}{}{}{}'.format((downlinkBits), (capabilityBits), (ICAO), (data), (newPI)))
    if args.mode == 'print':
        data = '*{};'.format(data)
        printData(data)
    elif args.mode == 'send':
        data = '*{};\n'.format(data)
        sendData(data)


#Send the ODD Airborn Position packet. See: https://mode-s.org/decode/content/ads-b/3-airborne-position.html
def sendoAP(ICAO, latLon):
    tcALT = '010110001100001110000'
    fBit = '1'
    lat = latLon[0]
    lon = latLon[1]
    lat, lon = cpr_encode(lat, lon, 1)
    lat = '{:17b}'.format((lat))
    lon = '{:17b}'.format((lon))
    lat = lat.replace(' ', '0')
    lon = lon.replace(' ', '0')
    latLon = '{}{}'.format(lat, lon)
    data = '{}{}{}'.format(tcALT, fBit, latLon)
    newPI = crc('{}{}{}{}{}'.format((downlinkBits), (capabilityBits), (ICAO), (data), (PI)))
    data = bin2hex('{}{}{}{}{}'.format((downlinkBits), (capabilityBits), (ICAO), (data), (newPI)))
    if args.mode == 'print':
        data = '*{};'.format(data)
        printData(data)
    elif args.mode == 'send':
        data = '*{};\n'.format(data)
        sendData(data)

data = ''


listOfICAOs = []
newLOD = []
listOfDirections = []
listOfLocations = []


# Generate ICAOs, mandatory 24 bit field. i is number of ICAOs to generate
def generateICAOs(i):
    for _ in range(i):
        ICAO = '{:24b}'.format(random.randint(0,16777215))
        ICAO = ICAO.replace(' ', '0')
        listOfICAOs.append(ICAO)



# i:+int - Number of planes to generate coordinate pairs for. point:[lat,lon] - starting coordinate to spawn planes from. 
# range:+int - number of degrees from point in which planes can be spawned
def generateDirections(i, point, distance):
    for _ in range(i):
        listOfDirections.append([point[0] + random.uniform((0 - distance), distance), point[1] + random.uniform((0 - distance), distance)])




def freedom(start_coord=[33.55462, -82.00394]):
    
    start_points = []
    coords = []
    for i in line(start_coord, 10, 'down', 7):
        start_points.append(i)
        coords.append(i)
    for count, i in enumerate(start_points):
        for inner, j in enumerate(line(i, 19, 'right', 19)):
            if count in [0,2,3,4,5,6]:
                coords.append(j)
            elif inner not in range(1,8):
                coords.append(j)


    coords += drawInput('USA', [32.70462, -82.253914])
    generateICAOs(len(coords))
    run(coords, 0, 0.5)

def smiley():

    smileyCoordsStart = [
    [33.4473, -82.0588], 
    [33.442248293369744, -82.03972338092676],
    [33.44259896297717, -82.01534914680676] , 
    [33.44961205745025, -81.999379821004], 
    [33.46153301666391, -82.03299945427298], 
    [33.478535077700634, -82.04854853465987], 
    [33.48046293588921, -82.01892123284159]]
    
    smileyRealCoords = []


    newLOD = deepcopy(listOfDirections)
    smileyCoords = deepcopy(smileyCoordsStart)
    count = 0
    generateICAOs(len(smileyCoords))
    while True:
        smileyCoords = deepcopy(smileyCoordsStart)
        for i in range(15):
            time.sleep(.2)
            for i in enumerate(listOfICAOs):
                if count ==  len(smileyCoords):
                    count = 0
                sendAV(i[1])
                sendeAP(i[1], smileyCoords[count])
                smileyCoords[count][1] += .01
                sendoAP(i[1], smileyCoords[count])
                
                count += 1
        for i in range(15):
            time.sleep(.2)
            for i in enumerate(listOfICAOs):
                if count ==  len(smileyCoords):
                    count = 0
                sendAV(i[1])
                sendeAP(i[1], smileyCoords[count])
                smileyCoords[count][0] += .01
                sendoAP(i[1], smileyCoords[count])
                
                count += 1
        for i in range(15):
            time.sleep(.2)
            for i in enumerate(listOfICAOs):
                if count ==  len(smileyCoords):
                    count = 0
                sendAV(i[1])
                sendeAP(i[1], smileyCoords[count])
                smileyCoords[count][1] -= .01
                sendoAP(i[1], smileyCoords[count])
                
                count += 1
        for i in range(15):
            time.sleep(.2)
            for i in enumerate(listOfICAOs):
                if count == len(smileyCoords):
                    count = 0
                sendAV(i[1])
                sendeAP(i[1], smileyCoords[count])
                smileyCoords[count][0] -= .01
                sendoAP(i[1], smileyCoords[count])
                count += 1
            


def normie():
    
    normieCoordsStart = [[33.55462, -82.00394], [33.48822, -82.12891], [33.48364, -81.8776], [33.38966, -81.79932], [33.38966, -81.79932], [33.4103, -82.16462]]
    
    normieCoords = deepcopy(normieCoordsStart)
    count = 0
    generateICAOs(len(normieCoords))
    run(normieCoords, 0.01, 0)
    

def glitch():

    generateICAOs(args.count)
    generateDirections(args.count, [33.4735277, -82.0097233], 10 * NM) # starting point over Augusta, range of 10 NM
    
    while True:
    
        for count, ICAO in enumerate(listOfICAOs):
            
            sendAV(ICAO)
            sendeAP(ICAO, listOfDirections[count])
            sendoAP(ICAO, listOfDirections[count])
            listOfDirections[count][0] += random.uniform(0 - NM, NM)
            listOfDirections[count][1] += random.uniform(0 - NM, NM)
        
        time.sleep(0.4)

        
def santa():

    generateICAOs(10000)
    generateDirections(10000, [90.0, 135.0], NM) # starting point over North Pole
    
    while True:
    
        for count, ICAO in enumerate(listOfICAOs):
            sendAV(ICAO)
            sendeAP(ICAO, listOfDirections[count])
            sendoAP(ICAO, listOfDirections[count])
            listOfDirections[count][0] += random.uniform(0 - NM, NM)
            listOfDirections[count][1] += random.uniform(0 - NM, NM)


# Default to drwaing over Augusta
def drawInput(text, start_coord=[33.55462, -82.00394]):
    coords = []
    save = deepcopy(start_coord)
    for i in text:
        savePoint = []
        start_coord[1] += args.scale * 2
        if i.upper() == 'A':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # Draw Left side
                if count == 0:
                    continue
                coords.append(i)
            for count, i in enumerate(line(coords[-4], args.scale, 'right', 5)): # Draw middle of A
                coords.append(i)
            for count, i in enumerate(line(start_coord, args.scale, 'right', 5)): # Draw top of A
                if count == 0:
                    continue
                if count == 4:
                    savePoint = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint, args.scale, 'down', 7)): # Draw Right Side of A
                if count == 0:
                    continue
                coords.append(i)
            continue
        if i.upper() == 'B':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # Draw left of B
                if count == 3:
                    savePoint3 = i
                if count == 6:
                    savePoint6 = i
                coords.append(i)
            for count, i in enumerate(line(start_coord, args.scale, 'right', 5)): # Draw top of B
                if count == 4:
                    savePoint4 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint4, args.scale, 'down', 7)): # Draw right of B
                if count in [1,2,4,5]:
                    coords.append(i)
            for count, i in enumerate(line(savePoint3, args.scale, 'right', 5)): # Draw middle of B
                if count == 4:
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint6, args.scale, 'right', 5)): # Draw bottom of B
                if count == 4:
                    continue
                coords.append(i)
            continue
        if i.upper() == 'C':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # Draw left of C
                if count in [0,6]:
                    if count == 0:
                        savePoint0 = i
                    if count == 6:
                        savePoint6 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint0, args.scale, 'right', 5)): # Draw top of C
                if count in [0,4]:
                    if count == 4:
                        savePoint4 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint4, args.scale, 'down', 7)): # Draw right of C
                if count in [1, 5]:
                    coords.append(i)
            for count, i in enumerate(line(savePoint6, args.scale, 'right', 5)): # Draw bottom of C
                if count in [0,4]:
                    continue
                coords.append(i)
            continue
        if i.upper() == 'D':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # Draw left of D
                if count == 6:
                    savePoint6 = i
                coords.append(i)
            for count, i in enumerate(line(start_coord, args.scale, 'right', 5)): # Draw top of D
                if count == 4:
                    savePoint4 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint6, args.scale, 'right', 5)): # Draw bottom of D
                if count == 4:
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint4, args.scale, 'down', 7)): # Draw right of D
                if count in [0,6]:
                    continue
                coords.append(i)
            continue
            
        if i.upper() == 'E':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # Draw left of E
                if count == 3:
                    savePoint3 = i
                coords.append(i)
            for count, i in enumerate(line(coords[-1], args.scale, 'right', 5)): # Draw bottom of E
                coords.append(i)
            for count, i in enumerate(line(start_coord, args.scale, 'right', 5)): # Draw top of E
                coords.append(i)
            for count, i in enumerate(line(savePoint3, args.scale, 'right', 5)): # Draw middle of E
                if count == 4:
                    continue
                coords.append(i)
            continue    
        if i.upper() == 'F':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # Draw left of F
                if count == 3:
                    savePoint3 = i
                coords.append(i)
            for count, i in enumerate(line(start_coord, args.scale, 'right', 5)): # Draw top of F
                coords.append(i)
            for count, i in enumerate(line(savePoint3, args.scale, 'right', 5)): # Draw middle of F
                if count == 4:
                    continue
                coords.append(i)
            continue
        if i.upper() == 'G':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # Draw left of G
                if count == 0:
                    savePoint0 = i
                    continue
                if count == 6:
                    savePoint6 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint0, args.scale, 'right', 5)): # Draw top of G
                if count == 0:
                    continue
                if count == 4:
                    savePoint4 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint4, args.scale, 'down', 7)): # Draw right of G
                if count in [1,4,5]:
                    if count == 4:
                        savePoint4 = i
                    coords.append(i)
            for count, i in enumerate(line(savePoint4, args.scale, 'left', 5)): # Draw inside of G
                if count == 1:
                    coords.append(i)
            for count, i in enumerate(line(savePoint6, args.scale, 'right', 5)): # Draw bottom of G
                if count in [1,2,3]:
                    coords.append(i)

            continue

        if i.upper() == 'H':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # Draw left of H
                if count == 3:
                    savePoint3 = i
                coords.append(i)
            for count, i in enumerate(line(savePoint3, args.scale, 'right', 5)): # Draw left of H
                coords.append(i)
            for count, i in enumerate(line([start_coord[0], coords[-1][1]], args.scale, 'down', 7)): # Draw left of H
                coords.append(i)
            continue
        if i.upper() == 'I':
            for i in line(start_coord, args.scale, 'right', 3): # draw top of I
                coords.append(i)
            for i in line(coords[-2], args.scale, 'down', 6): # draw center column
                coords.append(i)
            for i in line([coords[-1][0], start_coord[1]], args.scale, 'right', 3): # draw bottom of I
                coords.append(i)

            continue
        if i.upper() == 'J':
            for count, i in enumerate(line(start_coord, args.scale, 'right', 5)): # Draw top of J
                if count == 0:
                    continue
                if count == 4:
                    savePoint4 = i
                coords.append(i)
            for count, i in enumerate(line(savePoint4, args.scale, 'down', 7)): # Draw right of J
                if count == 6:
                    savePoint6 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint6, args.scale, 'left', 5)): # Draw bottom of J
                if count in [1,2,3]:
                    coords.append(i)
                if count == 4:
                    savePoint4 = i
            for count, i in enumerate(line(savePoint4, args.scale, 'up', 7)): # Draw left of J
                if count == 1:
                    coords.append(i)
            continue
        if i.upper() == 'K':
            start_points = []
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # draw left side 
                start_points.append(i)
                coords.append(i) # draw top right of y
                for count, i in enumerate(start_points):
                    for inner, j in enumerate(line(i, args.scale, 'right', 5)):
                        if count in [0,6]:
                            if inner == 4:
                                coords.append(j)
                        if count in [1,5]:
                            if inner == 3:
                                coords.append(j)
                        if count in [2,4]:
                            if inner == 2:
                                coords.append(j)
                        if count == 3:
                            if inner == 1:
                                coords.append(j)
            continue
        if i.upper() == 'L':
            for i in line(start_coord, args.scale, 'down', 7): # draw left side
                coords.append(i)
            for i in line(coords[-1], args.scale, 'right', 4): # draw from bottom point to the right
                coords.append(i)
            continue
        if i.upper() == 'M':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # Draw left of M
                if count == 0:
                    savePoint0 = i
                if count == 1:
                    savePoint1 = i
                if count == 2:
                    savePoint2 = i
                coords.append(i)
            for count, i in enumerate(line(savePoint1, args.scale, 'right', 5)): # Draw middle of M
                if count in [1,3]:
                    coords.append(i)
            for count, i in enumerate(line(savePoint2, args.scale, 'right', 5)): # Draw middle of M
                if count in [2,4]:
                    if count == 4:
                        savePoint4 = i
                        continue
                    coords.append(i)
                    
            for count, i in enumerate(line([start_coord[0],savePoint4[1]],args.scale, 'down', 7)): # Draw middle of M
                coords.append(i)
            continue
        if i.upper() == 'N':
            start_points = []
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # draw left side 
                start_points.append(i)
                coords.append(i) # draw top right of y
                for count, i in enumerate(start_points):
                    for inner, j in enumerate(line(i, args.scale, 'right', 5)):
                        if count in [0,1,5,6]:
                            if inner == 4:
                                coords.append(j)
                        if count == 2:
                            if inner in [1,4]:
                                coords.append(j)
                        if count == 3:
                            if inner in [2,4]:
                                coords.append(j)
                        if count == 4:
                            if inner in [3,4]:
                                coords.append(j)
            continue
        if i.upper() == 'O':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # Draw left of M
                if count == 0:
                    savePoint0 = i
                    continue
                if count == 6:
                    savePoint6 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint0, args.scale, 'right', 5)): # Draw left of M
                if count in [0,4]:
                    if count == 4:
                        savePoint4 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint6, args.scale, 'right', 5)): # Draw left of M
                if count in [0,4]:
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint4, args.scale, 'down', 7)): # Draw left of M
                if count in [0,6]:
                    continue
                coords.append(i)
            continue
        if i.upper() == 'P':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # Draw left of M
                if count == 0:
                    savePoint0 = i
                if count == 3:
                    savePoint3 = i
                coords.append(i)
            for count, i in enumerate(line(savePoint0, args.scale, 'right', 5)): # Draw left of M
                if count == 4:
                    savePoint4 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint3, args.scale, 'right', 5)): # Draw left of M
                if count == 4:
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint4, args.scale, 'down', 7)): # Draw left of M
                if count in [1,2]:
                    coords.append(i)
            continue
            
        if i.upper() == 'Q':
            start_points = []
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # draw left side 
                start_points.append(i)
                if count in [1,2,3,4,5]:
                    coords.append(i) # draw top right of y
                for count, i in enumerate(start_points):
                    for inner, j in enumerate(line(i, args.scale, 'right', 5)):
                        if count == 0:
                            if inner in [1,2,3]:
                                coords.append(j)
                        if count in [1,2,3]:
                            if inner == 4:
                                coords.append(j)
                        if count == 4:
                            if inner in [2,4]:
                                coords.append(j)
                        if count == 5: 
                            if inner in [3,4]:
                                coords.append(j)
                        if count == 6:
                            if inner in [1,2,3,4]:
                                coords.append(j)
            continue
        if i.upper() == 'R':
            start_points = []
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # draw left side 
                start_points.append(i)
                coords.append(i) # draw top right of R
                for count, i in enumerate(start_points):
                    for inner, j in enumerate(line(i, args.scale, 'right', 5)):
                        if count in [0,3]:
                            if inner in [0,1,2,3]:
                                coords.append(j)
                        if count in [1,2]:
                            if inner == 4:
                                coords.append(j)
                        if count == 4:
                            if inner == 2:
                                coords.append(j)
                        if count == 5:
                            if inner == 3:
                                coords.append(j)
                        if count == 6:
                            if inner == 4:
                                coords.append(j)
            continue
        if i.upper() == 'S':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)):
                if count in [0,3,4,6]:
                    if count == 0:
                        savePointTop = i
                    if count == 3:
                        savePointMid = i
                    if count == 6:
                        savePointBottom = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePointTop, args.scale, 'right', 5)):
                if count in [0,4]:
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePointMid, args.scale, 'right', 5)):
                if count in [0,4]:
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePointBottom, args.scale, 'right', 5)):
                if count in [0,4]:
                    if count == 4:
                        savePointRight = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePointRight, args.scale, 'up', 7)):
                if count in [0,3,4,6]:
                    continue
                coords.append(i)
            continue
        if i.upper() == 'T':
            for count, i in enumerate(line(start_coord, args.scale, 'right', 5)):
                coords.append(i)
            for count, i in enumerate(line(coords[-3], args.scale, 'down', 7)):
                coords.append(i)
            continue
        if i.upper() == 'U':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)):
                if count == 6:
                    savePoint = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint, args.scale, 'right', 5)):
                if count == 4:
                    savePoint = i
                    continue
                if count == 0:
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint, args.scale, 'up', 7)):
                if count == 0:
                    continue
                coords.append(i)
            continue
        if i.upper() == 'V':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)):
                if count == 5:
                    savePoint5 = i
                    continue
                if count == 6:
                    savePoint6 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint5, args.scale, 'right', 5)):
                if count in [0,2,4]:
                    continue
                coords.append(i)
                
            for count, i in enumerate(line(savePoint6, args.scale, 'right', 5)):
                if count == 2:
                    coords.append(i)
                if count == 4:
                    savePoint4 = i
            for count, i in enumerate(line(savePoint4, args.scale, 'up', 7)):
                if count in [0,1]:
                    continue
                coords.append(i)
            
            continue
        if i.upper() == 'W':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)):
                if count == 4:
                    savePoint4 = i
                if count == 5:
                    savePoint5 = i
                if count == 6:
                    savePoint6 = i
                    continue
                
                coords.append(i)
            for count, i in enumerate(line(savePoint4, args.scale, 'right', 5)):
                if count == 2:
                    coords.append(i)
            for count, i in enumerate(line(savePoint5, args.scale, 'right', 5)):
                if count == 2:
                    coords.append(i)
            for count, i in enumerate(line(savePoint6, args.scale, 'right', 5)):
                if count in [1,3]:
                    coords.append(i)
                if count == 4:
                    savePoint4 = i
            for count, i in enumerate(line(savePoint4, args.scale, 'up', 7)):
                if count == 0:
                    continue
                coords.append(i)
            continue

        if i.upper() == 'X':
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)):
                if count == 3:
                    savePoint3 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line(savePoint3, args.scale, 'right', 5)):
                if count == 0:
                    continue
                if count == 4:
                    savePoint4 = i
                    continue
                coords.append(i)
            for count, i in enumerate(line([start_coord[0], savePoint4[1]], args.scale, 'down', 7)):
                if count == 3:
                    continue
                coords.append(i)
            continue
        if i.upper() == 'Y': # Not finished
            start_points = []
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # draw left side 
                start_points.append(i)
                if count in [0,1,2]:
                    coords.append(i) # draw top right of y
                for count, i in enumerate(start_points):
                    for inner, j in enumerate(line(i, args.scale, 'right', 5)):
                        if count in [0,1,2]:
                            if inner == 4: #  draw top left side of y
                                coords.append(j)
                        if count == 3:
                            if inner in [1,2,3]: # draw 3 middle points of y
                                coords.append(j)
                        if count in [4,5,6]: # draw bottom of y
                            if inner == 2:
                                coords.append(j)
            continue
        if i.upper() == 'Z':
            start_points = []
            for count, i in enumerate(line(start_coord, args.scale, 'down', 7)): # draw left side 
                start_points.append(i)
                if count in [0,5,6]:
                    coords.append(i)
            for count, i in enumerate(start_points):
                for inner, j in enumerate(line(i, args.scale, 'right', 5)):
                    if count == 0: # First row, skip left most plane since it is already drawn
                        if inner == 0:
                            continue
                    if count == 1:
                        if inner in [0,1,2,3]:
                            continue
                    if count == 2:
                        if inner in [0,1,2,4]:
                            continue
                    if count == 3:
                        if inner in [0,1,3,4]:
                            continue
                    if count == 4:
                        if inner in [0,2,3,4]:
                            continue
                    if count == 5:
                            continue
                    if count == 6:
                        if inner == 0:
                            continue
                    coords.append(j)   
            continue
        
        if i.isspace():
            pass

        if i == '\\':
            save[0] -= args.scale * 2
            start_coord = deepcopy(save)

    return coords

def run(coords, moveNS = 0, moveWE = 0):
    
    generateICAOs(len(coords))

    while True:
        coords = deepcopy(coords)
        for i in range(len(coords)):
            ICAO = listOfICAOs[i]
            sendAV(ICAO)
            sendoAP(ICAO, coords[i])
            sendeAP(ICAO, coords[i])
            coords[i][0] += moveNS
            coords[i][1] += moveWE
            
        time.sleep(.5)
        

def main():
    if args.type == 'freedom':
        freedom()
    elif args.type == 'smiley':
        smiley()
    elif args.type == 'glitch':
        glitch()
    elif args.type == 'santa':
        santa()
    elif args.type == 'input':
        run(drawInput(args.input))
    else:

        normie()

main()